# == Schema Information
#
# Table name: orders
#
#  id         :bigint           not null, primary key
#  subtotal   :decimal(, )
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class Order < ApplicationRecord
  has_many :order_items
  # Add any method you need
end
