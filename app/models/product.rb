# == Schema Information
#
# Table name: products
#
#  id               :bigint           not null, primary key
#  inventory_amount :integer
#  name             :string
#  price            :decimal(, )
#  product_type     :string
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#
class Product < ApplicationRecord
  enum product_types: {simple: 'simple', bundle: 'bundle'}

end
