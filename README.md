## Objective
You’re going to build a backend of a simplistic Ecommerce store (no UI needed).  The Ecommerce store should be very simplistic
The objective:
Update an database infrastructure to work with bundled product
Build the functionality/methods to perform the basic shopping experience
Write test to validate the soundness of your design


## Description

### Product

There are two type of products:
* **Simple product**:  It’s nothing special, a user can add to their cart and expect the price
* **Bundled product**.  A bundled product is like a virtual product that contains child product(s).  A child product reference to Simple product.  A bundled product is considered sold out if there’s not enough child product to complete 1 bundled product.


An example of Bundle Product is
* Bundle Product: “Nintendo Switch Bundle Special”
  * 1 x Nintendo Switch
  * 2 x Nintendo Switch Controllers
  * 1 x Mario Game


Purchasing 2 of “Nintendo Switch Bundle Special” will subtract from the following inventory
* 2 x Nintendo Switch
* 4 x Nintento Switch Controller
* 2 x Mario Game

### Price
The price for bundle product can be set in multiple ways.
* **Fixed price**. The price is set regardless of the pricing of the child products
* **Percentage** discount of total child products’ price. (Eg: if the price of all the child products of “Nintendo Switch Bundle Special” is $400 and the percentage is set to 10%, then the bundled product price is $360.

## Functions
Here's some function that is needed: 
* **addToCart**(cart, product, quantity)
  * should be able to add Simple or Bundled Product
* **removeFromCart**(cart, product, quantity)
  * should be able to remove Simple or Bundled Product
* **cartSubtotal**()
  * Grabs the subtotal of the current cart
* **purchaseCart**()
  * Creates an Order
  * Subtract the product’s inventory
  * Empty or remove the cart
  * Should not be able to move forward if cart doesn't have enough or is out of stock
* You can add more functions if needed


## Expectation
Here’s a pseudocode of an example I’d like to see in a test


`P = Product.create(name: “Red Shirt”, product_type: 'simple', price: 2, inventory: 100) # This is just an example. Be sure to have a way to create Bundled Product as well

addToCart(p, 10)

expect(cartSubtotal) == 20

purchaseCart()

expect(p.inventory) == 80
`

Use RSpec to write test (see /spec folder)

## Tip
I created a skeleton to minimize the guess work.  Please review all the model files, I annotate it for it to be easy to understand the structure.  Good luck!