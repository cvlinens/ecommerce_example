class AddProductType < ActiveRecord::Migration[6.0]
  def change
    add_column :products, :product_type, :string, index: true
  end
end
